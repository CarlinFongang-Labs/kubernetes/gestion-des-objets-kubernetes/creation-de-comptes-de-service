# Création de comptes de service : Service Account


------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectifs

Nous allons parler de la création de comptes de service. 

Nous commencerons par expliquer ce 

1. qu'est un compte de service, puis nous aborderons 
2. le processus de création de comptes de service. Ensuite, nous parlerons du 
3. processus de liaison des rôles aux comptes de service


# 1. Qu'est-ce qu'un Compte de Service ?

Sous Kubernetes, un compte de service (Service Account) est un compte utilisé par les processus des conteneurs au sein des pods pour s'authentifier auprès de l'API Kubernetes. Si vos pods doivent communiquer avec l'API, vous pouvez utiliser des comptes de service pour contrôler leur accès.

# 2. Création de Comptes de Service

La création de nouveaux comptes de service est assez simple. Nous pouvons créer des objets de comptes de service grace à du **YAML**, comme n'importe quel autre objet Kubernetes. 
Voici un exemple rapide de YAML pour un compte de service. 

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: my-servicesaccount
```

Le contrôle d'accès pour les comptes de service est géré de la même manière que pour un compte utilisateur classique comme nous cela a été décrit dans [l'atelier précédent](https://gitlab.com/CarlinFongang-Labs/kubernetes/gestion-des-objets-kubernetes/lab-gestion-du-controle-d-acces-base-sur-les-roles-rbac.git), en utilisant des objets de contrôle d'accès basé sur les rôles (RBAC). Nous pouvons lier des comptes de service avec des **ClusterRoles** ou des **ClusterRoleBindings** pour fournir en accès des fonctionnalités de **l'API Kubernetes** à nos pods en utilisant ces comptes de service.

Voici un exemple rapide de ce à quoi cela ressemble de lier un rôle à un compte de service en utilisant un RoleBinding. 

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: sa-pod-reader
subjects: #<!Début de la liaison (StartBind)
- kind: ServiceAccount
  name: my-serviceaccount
  namespace: default #<!Fin de la liaison (EndBind)
roleRef:
  kind: Role
  name: pod-reader
  apiGroup: rbac.authorization.k8s.io
```

Il suffit de définir le compte de service comme sujet de notre liaison.

# Démonstration Pratique
Voyons maintenant une brève démonstration pratique de l'utilisation des comptes de service dans notre cluster Kubernetes. 

## 1. Connexion au nœud de contrôle de Kubernetes, 

la première chose à faire est de créer un compte de service. 

## 2. Création de Service Account

1. Créons un fichier YAML nommé **my-serviceaccount.ym**l** 

```yaml
nano my-serviceaccount.yml
```

et collons-y du YAML basique pour un compte de service, qui sera appelé **my-serviceaccount**.


```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: my-servicesaccount
```

2. Créons à présent le service

Les comptes de service existent dans des namespaces, donc ce compte sera placé dans le namespace par défaut. Mais, il n'est pas nécessaire de spécifier le namespace si c'est le namespace par défaut. Si vous souhaitez spécifier un namespace dans le YAML, cela peut être fait comme pour n'importe quel autre objet sous la section metadata.

Enregistrez et quittez le fichier, puis créez le compte de service avec

```yaml
kubectl create -f my-serviceaccount.yml
```

![Alt text](image.png)
*Service Account crée*

Il existe une méthode rapide et facile pour créer des comptes de service en utilisant **kubectl create sa**. 

Par exemple :

```yaml
kubectl create sa my-serviceaccount2 -n default
```
pour en créer un second. 
On peut aussi spécifier un namespace avec **-n**. 

![Alt text](image-1.png)
*Le second Service Account à bien été crée*

Pour vérifier les comptes de service

```yaml
kubectl get sa
```

![Alt text](image-2.png)
*Liste des Services Account crée*

Nous pouvez voir les deux comptes de service créés.


## 3. RoleBinding : liaision avec le ServiceAccount

Ensuite, créons un **RoleBinding** pour lier un rôle à ce compte. 

1. Création du fichier de binding

Créez un fichier nommé **sa-pod-reader.yml** pour le RoleBinding et collez-y la définition YAML. 

```yaml
nano sa-pod-reader.yml
```

contenu : 

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: sa-pod-reader
  namespace: default
subjects:
- kind: ServiceAccount
  name: my-serviceaccount
  namespace: default
roleRef:
  kind: Role
  name: pod-reader
  apiGroup: rbac.authorization.k8s.io
```


Il s'agit d'un **RoleBinding** simple appelé **sa-pod-reader** dans le **namespace** par défaut. La section **roleRef** fait référence à un rôle appelé pod-reader créé dans un laboratoire précédent. La section subject indique le type **ServiceAccount**, le nom du compte de service et le **namespace** où il réside. 

2. Cration du RoleBinding

```bash
kubectl create -f sa-pod-reader.yml
```

![Alt text](image-3.png)
*Création du RoleBinding réussie*

Ainsi, des permissions sont associées à ce compte de service via le **RoleBinding**. 

Comme pour d'autres objets, nous pouvons utilier la commande 

```bash
kubectl describe sa my-serviceaccount
```
 pour obtenir des informations supplémentaires sur le compte de service.

![Alt text](image-4.png)
*Description du Service Account my-serviceaccount*


# Résumé du Laboratoire

Ce laboratoire présente une brève démonstration pratique de la création de comptes de service et de la liaison des permissions à ceux-ci avec le contrôle d'accès basé sur les rôles. Nous avons présenté ce que sont les comptes de service, le processus de création de comptes de service, le processus de liaison des rôles aux comptes de service, et avons effectué une démonstration pratique.


# Reférences


[https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/]()

[Usage des autorisation RBAC :](https://kubernetes.io/docs/reference/access-authn-authz/rbac/)

